%{

/* A parser that collects data for a given object prefix */

var collect = []
var prefix = []

let prefix_emit = function (prefix) {
  return prefix.length === 2 && prefix[0] === 'rows' && typeof prefix[1] === 'number'
}

var collect_data = false

let top = (a) => a[a.length-1]

let enter = function (name) {
  prefix.push(name)
  collect.push(prefix_emit(prefix))
  collect_data = collect.some( (x) => x )
}

let next = function () {
  let n = prefix.pop()
  collect.pop()
  enter(n+1)
}

let leave = function () {
  prefix.pop()
  collect.pop()
  collect_data = collect.some( (x) => x )
}

let send = function(value) {
  console.log(top(collect)?'send':'(send)',collect,prefix,JSON.stringify(value))
  if(top(collect)) {
    emit({prefix,value})
  }
}

%}

%%

start
  : value { if(collect_data){ $$ = $1 } else { $$ = undefined }; send($1) }
  ;

value
  : object   { $$ = $1 }
  | array    { $$ = $1 }
  | NUMBER   { $$ = parseFloat($1) }
  | STRING   { $$ = $1 }
  | TRUE     { $$ = true }
  | FALSE    { $$ = false }
  | NULL     { $$ = null }
  ;
object
  : start_object pairs '}' { $$ = collect_data ? $2 : undefined; /* leave(); */ send($2) }
  ;
start_object
  : '{'     { /* enter('.') */ }
  ;
pairs
  :          { $$ = collect_data ? {} : undefined }
  | pair     { $$ = collect_data ? $1 : undefined }
  | pairs ',' pair { $$ = collect_data ? Object.assign($1,$3) : undefined }
  ;
pair
  : start_pair value { if(collect_data){ $$ = new Object(); $$[$1] = $2 } else { $$ = undefined }; send($2); leave() }
  ;
start_pair
  : STRING ':' { enter($1); $$ = $1 }
  ;

array
  : start_array values ']'  { $$ = collect_data ? $2 : undefined; leave(); send($2) }
  ;
start_array
  : '['        { enter(0) }
  ;

values
  :            { $$ = collect_data ? [] : undefined }
  | value      { next(); $$ = collect_data ? [$1] : undefined }
  | values ',' value { next(); $$ = collect_data ? $1.concat($3) : undefined }
  ;

