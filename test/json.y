%%

// This example parses JSON and builds an in-memory datastructure.

start
  : value { $$ = $1; emit($$) }
  // works also with : value EOF { $$ = $1 } if the lexer generates EOF
  ;

value
  : object   { $$ = $1 }
  | array    { $$ = $1 }
  | NUMBER   { $$ = parseFloat($1) }
  | STRING   { $$ = $1 }
  | TRUE     { $$ = true }
  | FALSE    { $$ = false }
  | NULL     { $$ = null }
  ;
object
  : '{' pairs '}' { $$ = $2 }
  ;
pairs
  :          { $$ = {} }
  | pair     { $$ = $1 }
  | pairs ',' pair { $$ = Object.assign($1,$3) }
  ;
pair
  : STRING ':' value { $$ = new Object(); $$[$1] = $3 }
  ;
array
  : '[' values ']'  { $$ = $2 }
  ;
values
  :            { $$ = [] }
  | value      { $$ = [$1] }
  | values ',' value { $$ = $1.concat([$3]) }
  ;

